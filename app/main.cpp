#include "student.h"
#include "whitelist.h"
#include <iostream>
void enter_data_into_whitelist(whitelist &student_list);

int main()
{

    std::string name;
    whitelist student_list;
    student *stu_ptr;

    enter_data_into_whitelist(student_list);
    
    std::cout << "\n\n\n========================================\n";
    std::cout << "\tAdvanced C++ Concepts Task\n";
    std::cout << "========================================\n";
    std::cout << "Enter name to search from whitelist: ";
    std::cin >> name;

    if (student_list.is_student_present(name))
    {
        stu_ptr = student_list.get_student_data(name);

        std::cout << "\nRoll no. of " << name << " : " << stu_ptr->get_roll_no() << std::endl;
        std::cout << "Age of " << name << " : " << stu_ptr->get_age() << std::endl;
        std::cout << "CGPA of " << name << " : " << stu_ptr->get_cgpa() << std::endl;
    }
    else
    {
        std::cout << "Name ' " << name <<" ' not whitelisted\n";
    }
    std::cout << "\n========================================\n";
}

void enter_data_into_whitelist(whitelist &student_list)
{

    student stu_1("A","170488", 22, 4);
    student_list.add_to_whitelist("Jawad", stu_1);
    stu_1.set("B","170401", 22, 3.6);
    student_list.add_to_whitelist("Adil", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Ali", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Abdullah", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Umair", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Usman", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Usama", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Ahamed", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Talha", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Arim", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Haris", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Farid", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Hamza", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Yasir", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Munib", stu_1);
    stu_1.set("C","160421", 25, 2.5);
    student_list.add_to_whitelist("Haris", stu_1);
    
}