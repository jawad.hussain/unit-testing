# C++ Unit Testing

## Discription:
simple code that demonstrates unit testing using gtest library.

## How to Run:

1. clone library using command:
    
    ```
    git clone https://gitlab.com/jawad.hussain/unit-testing.git
    ```

2. Run the following commands from root of cloned folder:

    ```
    mkdir build
    cd build
    cmake ..
    make
    ```

3. To run test:
   
    ```
   ./test/test
    ```

4. To run code:

    ```
    ./app/main
    ```