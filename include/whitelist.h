#ifndef WHITELIST_H
#define WHITELIST_H

#include "student.h"
#include <list>
using namespace emumba::training;



class whitelist
{
private:
    std::map<std::string, student *> student_record;
    std::list<student> student_data_list;

public:
    void add_to_whitelist(const std::string ,const student &);
    bool is_student_present(const std::string &) const;
    student *get_student_data(const std::string &) const;
};

#endif