#include "student.h"
#include "whitelist.h"
#include "gtest/gtest.h"
using namespace emumba::training;


struct student_test : testing::Test
{
    student* st1;
    student_test()
    {
        st1 = new student("Jawad", "i170488", 22, 3.98);
    }
    virtual ~student_test()
    {
        delete st1;
    }
};

struct student_data
{
    std::string name;
    std::string roll_no;
    int age;
    float cgpa;
    friend std::ostream& operator<<(std::ostream& os, const student_data& obj)
  {
    return os
      << "Name: " << obj.name
      << "|| Roll no: " << obj.roll_no
      << "|| Age: " << obj.age
      << "|| CGPA: " << obj.cgpa;
  }

};

struct student_updated :student_test, testing::WithParamInterface<student_data>
{
    student_updated()
    {
        st1->set(GetParam().name,GetParam().roll_no,GetParam().age, GetParam().cgpa);
    }
};

INSTANTIATE_TEST_CASE_P(Default, student_updated,
  testing::Values(
  student_data{"Jawad","i170488",22,3.98},
  student_data{"Adil","i170481",23,3.97}
  ));

TEST_F(student_test, testing_whitelist_1)
{
    
    whitelist list1;
    list1.add_to_whitelist("Jawad", *st1);
    ASSERT_EQ(list1.is_student_present("Jawad"), true);
    ASSERT_EQ(list1.get_student_data("Adil"),nullptr);

}

TEST_P(student_updated, testing_student_1 )
{
    ;
    ASSERT_EQ(GetParam().name, st1->get_name());
    ASSERT_EQ(GetParam().age, st1->get_age());
    ASSERT_FLOAT_EQ(GetParam().cgpa , st1->get_cgpa());
    ASSERT_EQ(GetParam().roll_no, st1->get_roll_no());
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}