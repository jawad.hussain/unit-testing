#include "whitelist.h"
#include "student.h"
#include <iostream>
using namespace emumba::training;

void whitelist::add_to_whitelist(const std::string name,const student &data)
{
    student_data_list.push_back(data);
    if (is_student_present(name))
    {
        student_record[name] = &student_data_list.back();
        std::cout << "Student " << name << " was already whitelisted. Student record updated\n";
    }
    else
    {
        student_record[name] = &student_data_list.back();
        std::cout << "Student " << name << " has been whitelisted.\n";
    }

    return;
}

bool whitelist::is_student_present(const std::string &name) const
{
    if (student_record.find(name) != student_record.end())
    {
        return true;
    }
    else
    {
        return false;
    }
}

student *whitelist::get_student_data(const std::string &name) const
{
    auto it = student_record.find(name);
    if (it != student_record.end())
    {
        return it->second;
    }
    else
    {
        std::cout << "Student " << name << "is not whitelisted.\n";
        return NULL;
    }
}

/* If we must use vectors
whitelist::whitelist()
{
    student_data_list.reserve(200);
}
*/